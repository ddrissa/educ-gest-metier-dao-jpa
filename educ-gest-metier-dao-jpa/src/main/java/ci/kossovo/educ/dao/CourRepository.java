package ci.kossovo.educ.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import ci.kossovo.educ.entity.Cours;

public interface CourRepository extends JpaRepository<Cours, Long> {

}
