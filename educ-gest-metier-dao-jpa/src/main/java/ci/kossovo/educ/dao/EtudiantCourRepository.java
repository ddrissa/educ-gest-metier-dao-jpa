package ci.kossovo.educ.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import ci.kossovo.educ.entity.EtudiantCours;
import ci.kossovo.educ.entity.EtudiantCoursID;

public interface EtudiantCourRepository extends JpaRepository<EtudiantCours, EtudiantCoursID> {

}
