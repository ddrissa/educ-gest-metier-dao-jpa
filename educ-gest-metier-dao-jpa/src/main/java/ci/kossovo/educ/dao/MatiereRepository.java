package ci.kossovo.educ.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import ci.kossovo.educ.entity.Matiere;

public interface MatiereRepository extends JpaRepository<Matiere, Long> {

}
