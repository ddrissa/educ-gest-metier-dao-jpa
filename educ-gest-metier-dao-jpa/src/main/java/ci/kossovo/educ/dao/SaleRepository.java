package ci.kossovo.educ.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import ci.kossovo.educ.entity.Sale;

public interface SaleRepository extends JpaRepository<Sale, Long> {

}
