package ci.kossovo.educ.metier;

import ci.kossovo.educ.entity.Enseigne;
import ci.kossovo.educ.entity.EtudiantPomo;
import ci.kossovo.educ.entity.Promotion;

public interface IPromotionMetier extends IMetier<Promotion, Long> {
	public EtudiantPomo affecterEtudiant(Long idPromo, Long idEtudiant, int annee );
	public Enseigne affecterEnseigMat(Long idPromo, Long idEnseignant,Long idMatiere);

}
